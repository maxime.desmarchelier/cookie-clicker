# Cookie clicker


## Fournisseur et système

Fournisseur Azure - Hebérgé sur Debian 11 

## Première connexion à la machine virtuelle

Lors de la création de la machine virtuelle, Azure génère automatiquement une clé ssh permettant de s'y connecter
```
ssh -i cle.pem nosql@20.55.65.184
```

## Installation des packages nécessaire au serveur web et au déploiement
```
apt install apache2
apt install git
```
## On vérifie que le site est bien accessible de l'extérieur
```
curl http://20.55.65.184
```


## On déploie le code source via git
```
cd /var/www/
git clone https://gitlab.pedago.ensiie.fr/maxime.desmarchelier/cookie-clicker.git .
```